export class Resource {
    id: string;
    title: string;
     
    constructor(id: string, title:string) { 
        this.id = id;
        this.title = title;
    }
}

export class Task {
    id : string;
    title : string;
    startDate : Date;
    dueDate : Date;
    assignedTo : Resource;
    complete : number;
    
    constructor(id: string, title: string, startDate: Date, dueDate: Date, assignedTo: Resource, complete: number) {
        this.id = id;
        this.title = title;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.assignedTo = assignedTo;
        this.complete = complete;
    }
}

export class GroupedTasks {
    name : string;
    tasks : Task[];

    constructor(name: string, tasks: Task[]) {
        this.name = name;
        this.tasks = tasks;
    }
}