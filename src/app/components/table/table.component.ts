import { Component, Renderer2, group } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

import {GroupedTasks} from '../task'
import { DataService } from '../../services/data.service';

@Component({
    selector: 'resource-utilization-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

    groupedTasks : GroupedTasks [] ;

    constructor(private _dataService: DataService, private renderer: Renderer2) { }

    ngOnInit() {
        let self =this;
        
        // this._dataService.getTasks().then((tasks)=> {          
        //     self.groupedTasks = tasks;
        // });
        this._dataService.tasks.subscribe( tasks => this.groupedTasks = tasks);
    }

    addandlog() {
        let test : GroupedTasks = new GroupedTasks('new', new Array());
        this.groupedTasks.push(test);
        console.log("TableComponent");
        console.log(this.groupedTasks);
    }

    hideTasks(event) { 
        let c = event.target.parentElement.parentElement.children;
        for (let i=0; i<c.length; i++) {
            if (c[i].classList.contains("task") && c[i].classList.contains("open")) {
                c[i].classList.remove("open");
            }
            else if (c[i].classList.contains("task") && !c[i].classList.contains("open")) {
                c[i].classList.add("open");
            }
        }
    }
}



