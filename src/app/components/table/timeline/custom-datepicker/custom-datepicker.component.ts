import {Component} from '@angular/core';
import {Scale} from '../../../scale';
import { NotificationService } from '../../../../services/notification.service';


@Component({
  selector: 'custom-datepicker',
  templateUrl: './custom-datepicker.component.html',
  styleUrls: ['./custom-datepicker.component.css'],
})
export class CustomDatePicker {

  scale : Scale;
  
  startDate : Date;
  endDate : Date;
  selectedDate : Date;

  constructor(private _notificationService: NotificationService) { }

  ngOnInit() {
    
    this.selectedDate = new Date();
    this.endDate = new Date();
    this.startDate = new Date();

    this._notificationService.currentScale.subscribe( scale => { 
      this.scale = scale; 
      this.changeDate();   
    });
    
  }

  changeDate() { 
    switch (this.scale) {
      case Scale.Hours:
          this.startDate = new Date(this.selectedDate.getTime()-1000*60*60*24*2);
          this.endDate = new Date(this.selectedDate.getTime()+1000*60*60*24*2);
          break;
      case Scale.Days:
          this.startDate = new Date(this.selectedDate.getTime()-1000*60*60*24*40);
          this.endDate = new Date(this.selectedDate.getTime()+1000*60*60*24*40);
          break; 
    }
    this._notificationService.changeDates(this.startDate, this.endDate);  
  }

  timeForward(isForward: boolean) {
    let direction : number = (isForward) ? 1 : -1;
    switch (this.scale) {
      case Scale.Hours:
          this.selectedDate = new Date(this.selectedDate.getTime() + direction * 1000*60*60*24 * 3);
          break;
      case Scale.Days:
          this.selectedDate = new Date(this.selectedDate.getTime() + direction * 1000*60*60*24*80);
          break;
    }
    this.changeDate();
    
    this._notificationService.onClickChangeDates();
  }
}