import { Input, Component, group } from '@angular/core';

import "svg.js";
import { GroupedTasks, Task } from '../../task';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import {Scale} from '../../scale';
import { NotificationService } from '../../../services/notification.service';
import { DataService } from '../../../services/data.service';

import {SizeSVG} from './svg';

declare var SVG : any;

declare global { 
    interface Date {
        addDays(day : number) : Date;
        monthNames : string[];
        getMonthName() ;
        getShortMonthName() ;
    }

    interface Number {
        addPx() : string;
    }
}

Number.prototype.addPx = function () : string {
    return this.valueOf() + 'px';
}

Date.prototype.addDays = function(days: number) : Date {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

Date.prototype.getMonthName = function() {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};

@Component({
    selector: 'timeline',
    templateUrl: './timeline.component.html', 
    styleUrls: ['./timeline.component.css']
})
export class TimelineComponent {

    @Input('groupedTasks') groupedTasks: GroupedTasks[];

    private tasks: GroupedTasks[];

    private scale : Scale;

    private startDateOfScale : Date; 
    private endDateOfScale : Date;
    private startWeekValue : number; // начало недели от 0 до 6 (вс - сб)
    private sizeSVG : SizeSVG;

    private colorHeader : string = '#8C9EFF';
    private colorRow : string = '#E8EAF6';
    private colorHeaderStroke : string = '#E8EAF6';    

    constructor(private _notificationService: NotificationService, private _dataService: DataService) {}
    
    ngOnInit() {

        this._dataService.tasks.subscribe (tasks => this.tasks = tasks);

        let svgCanvas = SVG("svg");

        this.startWeekValue = 0; 

        this._notificationService.startDate.subscribe( startDate => this.startDateOfScale = startDate);
        this._notificationService.endDate.subscribe( endDate => this.endDateOfScale = endDate );

        this._notificationService.currentScale.subscribe( scale => {
            this.scale = scale;
            this.drawTimeline(svgCanvas);  
       });
    }

    private drawTimeline(svgCanvas: any) {
        svgCanvas.clear();
        this.setWidthCanvasSVG();
        this.drawLines(svgCanvas);
        this.drawHeader(this.scale, svgCanvas);
        
    }

    private setWidthCanvasSVG() {
        let countDays = this.getArrayDates(this.startDateOfScale, this.endDateOfScale).length;
        this.sizeSVG = new SizeSVG(this.scale, countDays, 25, 30);

        let _svg = document.getElementById("svg");
        if(_svg) {
            _svg.setAttribute("width", this.sizeSVG.getWidthCanvasSVG().addPx() ); // +"px");
        }
    }
    
    private drawLines(svgCanvas) {
        if (this.groupedTasks !== null) {
            let rect = svgCanvas.rect(this.sizeSVG.getWidthCanvasSVG(),this.sizeSVG.getHeigthRow()).attr({fill: this.colorRow, stroke:'#7f8ca5'}).move(0,75);
            let group = svgCanvas.group();
            for (let i=0, y=0; i<this.groupedTasks.length; i++, y+=25) {
                let use = svgCanvas.use(rect).move(0,y);  // for resource 
                group.add(use); // for resource 
                for (let j=0; j<this.groupedTasks[i].tasks.length; j++) {
                    y+=25;
                    svgCanvas.use(rect).move(0, y); // for tasks
                    this.drawTask(this.groupedTasks[i].tasks[j], svgCanvas, 0, y);
                }
            }
                
        }
    }

    private drawTask(task : Task, svgCanvas:any, moveX: number, moveY: number) {
        let rect = svgCanvas.rect(this.calcLengthTaskSVG(task)*this.sizeSVG.getWidthCellLowerHeader(), 10).fill("red").move(moveX,moveY);
    }

    private calcLengthTaskSVG(task: Task) {
        switch(this.scale) {
            case Scale.Hours:
                return Math.ceil(this.diffTime(task.startDate, task.dueDate)/1000/60/60); 
            case Scale.Days:
                return Math.ceil(this.diffTime(task.startDate, task.dueDate)/1000/60/60/24);
        }
    }

    private diffTime(date1: Date, date2:Date) {
        return Math.abs(date1.getTime()-date2.getTime()); //нет проверки на то чтобы дата1 была больше даты2, надо проверять на сервере!
    }
    
    public drawHeader(typeScale: Scale, svgCanvas) {
       
        let sizeSVG: SizeSVG;
        switch (typeScale) {
            case Scale.Hours:
                this.drawHeaderOfHourScale(svgCanvas); 
                break;
            case Scale.Days:
                this.drawHeaderOfDaysScale(svgCanvas); 
                break; 
        }
    }

    private drawHeaderOfDaysScale(svgCanvas) {
        let start = this.roundToFullWeekStart(this.startDateOfScale);
        let end = this.roundToFullWeekEnd(this.endDateOfScale);

        let arrayDates = this.getArrayDates(start, end);
        
        this.drawTopHeaderDayScale(start, end, arrayDates, svgCanvas);
        this.drawLowerHeaderDayScale(start, end, arrayDates, svgCanvas);
    }

    private drawTopHeaderDayScale(startDate, endDate, arr, svgCanvas) {
        
        for (let i=0 , x=0; i<arr.length; i++, x+=this.sizeSVG.getWidthCellLowerHeader()) {   
            if (this.isStartWeek(arr[i], this.startWeekValue)) { 
                let date = { 
                    month :arr[i].getMonthName(),
                    day : arr[i].getDate(), 
                    year : arr[i].getFullYear() 
                }
                let group = svgCanvas.group();
                let rect = svgCanvas.rect(this.sizeSVG.getWidthCellTopHeader(), this.sizeSVG.getHeigthRow())
                                    .attr({fill: this.colorHeader, stroke: this.colorHeaderStroke})
                                    .move(x, this.sizeSVG.getHeigthRow());
                let text = svgCanvas.text(date.month+' '+date.day+','+ date.year)
                                    .move(x, this.sizeSVG.getHeigthRow())
                                    .attr({ anchor: 'start'});
                group
                .add(rect)
                .add(text);
            }
        }

    }

    private drawLowerHeaderDayScale(startDate, endDate, arr, svgCanvas) {   
        let arrDate = this.getOnlyDayArray(arr);
        for (let i=0, x=0; i<arrDate.length; i++, x+=this.sizeSVG.getWidthCellLowerHeader()) {   
        var group = svgCanvas.group();
        var rect = svgCanvas.rect(this.sizeSVG.getWidthCellLowerHeader(), this.sizeSVG.getHeigthRow())
                                            .attr({ fill: this.colorHeader, stroke: this.colorHeaderStroke})
                                            .move(x, this.sizeSVG.getHeigthRow()*2);
            var text = svgCanvas.text(arrDate[i].toString())
                                .move(x, this.sizeSVG.getHeigthRow()*2)
                                .attr({ anchor: 'start'});
        group
        .add(rect)
        .add(text);
        }
    }

    private drawHeaderOfHourScale(svgCanvas) {
        let arrayOfDates = this.getArrayDates(this.startDateOfScale, this.endDateOfScale);
        this.drawTopHeaderHoursScale(arrayOfDates, svgCanvas);
        this.drawLowerHeaderHoursScale(arrayOfDates, svgCanvas);
    }
	
    private drawTopHeaderHoursScale(arrayOfDates, svgCanvas) {
        for (let i=0, x=0; i<arrayOfDates.length; i++, x+=this.sizeSVG.getWidthCellTopHeader()) {
            let group = svgCanvas.group();
            let rect = svgCanvas.rect(this.sizeSVG.getWidthCellTopHeader(), this.sizeSVG.getHeigthRow())
                                .attr({fill: this.colorHeader, stroke: this.colorHeaderStroke})
                                .move(x, this.sizeSVG.getHeigthRow());
            let text = svgCanvas.text('another new day')
                                .move(x, this.sizeSVG.getHeigthRow())
                                .attr({ anchor: 'start'});
            group.add(rect).add(text);
        }
    }

    private drawLowerHeaderHoursScale(arrOfDates, svgCanvas) {
        let arrOfDays = this.getOnlyDayArray(arrOfDates);
        let y = this.sizeSVG.getHeigthRow()*2;
        for (let i=0, x=0; i<arrOfDays.length; i++) {
            for(let j=0; j<24; j++, x+=this.sizeSVG.getWidthCellLowerHeader()) {
                let group = svgCanvas.group();
                let rect = svgCanvas.rect(this.sizeSVG.getWidthCellLowerHeader(), this.sizeSVG.getHeigthRow())
                                    .attr({ fill: this.colorHeader, stroke: this.colorHeaderStroke})
                                    .move(x, this.sizeSVG.getHeigthRow()*2);
                let text = svgCanvas.text(j.toString())
                                    .move(x,y);
            group.add(rect).add(text);
            }
        }
    }

    private roundToFullWeekStart(day) {
        let temp = day.getDay();
        if (temp === 0) return day;
        else return new Date(day.setDate(day.getDate() - temp));
    }

    private roundToFullWeekEnd(day) {
        let temp = day.getDay();
        if (temp === 6) return day;
        else return new Date(day.setDate(day.getDate() + (6 - temp)));
    }

    private getArrayDates(start, end) {
        let dateArray = new Array();
        let currentDate = start;
        while (currentDate <= end) {
            dateArray.push(new Date (currentDate));
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }

    private isStartWeek(curDay, startOfTheWeek) {  
        return curDay.getDay() === startOfTheWeek;
    }

    private getOnlyDayArray(arr) {   
        let dateArray = new Array();
        for (let i=0; i<arr.length; i++) {
            dateArray[i] = arr[i].getDate();
        }
        return dateArray;
    }

}