import {Scale} from '../../scale';

export class SizeSVG {
    private scale : Scale;
    private countDays: number; 
    private widthCanvasSVG: number;

    private heigthRow : number;
    private widthCellLowerHeader : number
    private widthCellTopHeader: number;

    constructor(scale:Scale, countDays:number, heigthRow: number, widthCellLowerHeader: number) {
        this.scale = scale;
        this.countDays = countDays;
        this.heigthRow = heigthRow;
        this.widthCellLowerHeader = widthCellLowerHeader; 

        this.widthCanvasSVG = this.calculateWidthCanvasSVG();
    }

    public getHeigthRow() : number {
        return this.heigthRow;
    }

    public getWidthCellLowerHeader() : number{
        return this.widthCellLowerHeader;
    }

    public getWidthCellTopHeader() : number {
        switch (this.scale) {
            case Scale.Hours:
                return this.widthCellTopHeader = 24 * this.widthCellLowerHeader;
            case Scale.Days:
                return this.widthCellTopHeader = 7 * this.widthCellLowerHeader;
        }
    }

    public getWidthCanvasSVG() : number {
        return this.widthCanvasSVG; 
    }

    private calculateWidthCanvasSVG() {
        switch(this.scale) {
            case Scale.Hours:
                return this.widthCellLowerHeader * this.countDays * 24;
            case Scale.Days:
                return this.widthCellLowerHeader * this.countDays;
        }
    }
}