import {Component} from '@angular/core';

import {Scale} from '../scale';
import { NotificationService } from '../../services/notification.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Output } from '@angular/core/src/metadata/directives';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

    scale : Scale;

    constructor(private _notificationService: NotificationService) { }
    
    ngOnInit() {
        this._notificationService.currentScale.subscribe( scale => this.scale = scale);
    }

    private changeScale(scale : Scale) {
        this._notificationService.changeScale(scale);
    }
}