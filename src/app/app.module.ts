import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule,  
        MatNativeDateModule, 
        MatCardModule, 
        MatButtonModule,
        MatInputModule,
        MatToolbarModule,
        MatMenuModule
        } from '@angular/material';

// import {MatTableDataSource} from '@angular/material';

import { AppComponent } from './app.component';
import {TableComponent} from './components/table/table.component';
import { DataService } from './services/data.service';
import { TimelineComponent } from './components/table/timeline/timeline.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { NotificationService } from './services/notification.service';
import {CustomDatePicker} from './components/table/timeline/custom-datepicker/custom-datepicker.component';


@NgModule({
  declarations: [
      AppComponent,
      TableComponent,
      TimelineComponent,
      ToolbarComponent,
      CustomDatePicker

  ],
  imports: [
    BrowserModule, 
    FormsModule,
    NoopAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatMenuModule
    // ,MatTableDataSource
    
  ],
  providers: [
    DataService, 
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
