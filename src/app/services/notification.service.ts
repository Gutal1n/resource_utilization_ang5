import { Injectable } from "@angular/core";
import { Scale } from "../components/scale";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class NotificationService {

    constructor() {}


    private _scaleSource = new BehaviorSubject<Scale>(Scale.Days);
    public currentScale = this._scaleSource.asObservable();

    public changeScale(scale: Scale) {
        this._scaleSource.next(scale);
    }

    private _startDate = new BehaviorSubject<Date>(new Date());
    public startDate = this._startDate.asObservable();

    private _endDate = new BehaviorSubject<Date>(new Date());
    public endDate = this._endDate.asObservable();

    public changeDates(startDate: Date, endDate: Date) {
        this._startDate.next(startDate);
        this._endDate.next(endDate);
    }

    public onClickChangeDates() {
        console.log("service change dates|| start:");
    }

}