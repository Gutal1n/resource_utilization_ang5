import {Injectable} from '@angular/core';
import {Task} from "../components/task";
import {GroupedTasks} from "../components/task";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {

    private _tasks = new BehaviorSubject<GroupedTasks[]>(this.groupTasksByResources(TASKS));
    public tasks = this._tasks.asObservable();

    // getTasks() {
    //     return Promise.resolve( this.groupTasksByResources(TASKS) );
    // }

    // getTest() {
    //     return [1,2,3,4,5];
    // }

    private getListOfUniqResourceNames (tasks: Task[]) {
        let uniqRes : string[] =[];
        for (let i=0; i<tasks.length; i++) {   
            if (uniqRes.indexOf(tasks[i].assignedTo.id) === -1)
                uniqRes.push(tasks[i].assignedTo.id); 
        }
        return uniqRes;
    }

    private groupTasksByResources(tasks: Task[]) : GroupedTasks[] {
        let uniqRes = this.getListOfUniqResourceNames(tasks);
        let group : GroupedTasks[] = new Array(uniqRes.length);

        for (let i=0; i<uniqRes.length; i++) {

            group[i] = new GroupedTasks(uniqRes[i], new Array())
            
            for (let j=0; j<tasks.length; j++) {
                if (tasks[j].assignedTo.id === uniqRes[i]) {
                    group[i].tasks.push(tasks[j]);
                }
            }        
        }
        return group;
    } 
}

export let TASKS: Task[] = [
    {
        id: "001", 
        title: "task 001",
        startDate: new Date("2017-11-01T12:40:00.000Z"),
        dueDate: new Date("2017-11-16T16:00:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 50 
        
    },
    {
        id: "002", 
        title: "task 002",
        startDate: new Date("2017-11-07T12:00:00.000Z"),
        dueDate: new Date("2017-12-14T13:38:00.000Z"),
        assignedTo:  {
            id: "null",
            title: "everyone"
        },
        complete: 90 
    },
    {
        id: "003", 
        title: "task 003",
        startDate: new Date("2017-11-15T12:00:00.000Z"),
        dueDate: new Date("2017-11-17T12:00:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 40 
    },
    {
        id: "004", 
        title: "task 004",
        startDate: new Date("2017-11-02T12:00:00.000Z"),
        dueDate: new Date("2017-11-06T12:00:00.000Z"),
        assignedTo: {
            id: "employee1",
            title: "everyone"
        },
        complete: 0 
    },
    {
        id: "005", 
        title: "task 005",
        startDate: new Date("2017-11-02T12:00:00.000Z"),
        dueDate: new Date("2017-11-07T12:00:00.000Z"),
        assignedTo: {
            id: "employee1",
            title: "everyone"
        },
        complete: 0 
    },
    {
        id: "006", 
        title: "task 006",
        startDate: new Date("2017-11-14T12:50:00.000Z"),
        dueDate: new Date("2017-11-14T21:00:00.000Z"),
        assignedTo: {
            id: "employee2",
            title: "everyone"
        },
        complete: 20 
    }
];